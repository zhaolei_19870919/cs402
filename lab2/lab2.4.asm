.data 0x10000000

var1: .word 99 # variable in data segment
var2: .word 19
.extern ext1 4
.extern ext2 4

.text
.globl main

main: addu $s0, $ra, $0 #save $31 in $16

lw $t0, var1 # load var1 to register t0
sw $t0, ext1 # store the value in register t1 to ext1
lw $t1, var2 # load var2 to register t1
sw $t1, ext2 # store the value in register t1 to ext2
la $t2, ext1 # load the memory address of ext1 to register t2
la $t3, ext2 # load the memory address of ext2 to register t3

addu $ra, $0, $s0 # return address back in $31
jr $ra # return from main