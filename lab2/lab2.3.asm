.data 0x10000000
var1: .word 0x10
var2: .word 0x20
var3: .word 0x30
var4: .word 0x40
first: .byte 'L'
last: .byte 'Z'

.text
.globl main
main: addu $16, $31, $0 # save $31 in $16

lui $1, 0x1000 #lw $t0, var1
lw $8, 0($1)

lui $1, 0x1000 #lw $t1, var2
lw $9, 4($1)

lui $1, 0x1000 #lw $t2, var3
lw $10, 8($1)

lui $1, 0x1000 #lw $t3, var4
lw $11, 12($1)

lui $1, 0x1000 #lw $t4, first
lb $12, 16($1)

lui $1, 0x1000 #lw $t5, last
lb $13, 17($1)

lui $1, 0x1000 #sw $t3, var1
sw $11, 0($1)

lui $1, 0x1000 #sw $t2, var2
sw $10, 4($1)

lui $1, 0x1000 #sw $t1, var3
sw $9, 8($1)

lui $1, 0x1000 #sw $t0, var4
sw $8, 12($1)

addu $31, $0, $16 # return address back in $31
jr $ra # return from main