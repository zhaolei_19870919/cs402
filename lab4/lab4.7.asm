.data
input_msg: .asciiz "enter two non-negative integers: "
output_msg: .asciiz "answer is: "
err_msg: .asciiz "one or more number(s) is negative. try again.\n"

.text
.globl main
main:	
	li $v0, 4		# display input message
	la $a0, input_msg
	syscall
	li $v0, 5		# read_int
	syscall
	move $t0, $v0		
	li $v0, 5		# read_int
	syscall
	move $t1, $v0		
	bltz $t0, err		# check whether input is negative
	bltz $t1, err
	j ok
err:
	li $v0, 4		# print error message
	la $a0, err_mag
	syscall
	j main			
ok:
	li $v0, 4		
	la $a0, output_msg	# print message
	syscall
	
	addi $sp, $sp, -4	# save $ra
	sw $ra, 4($sp)
	move $a0, $t0		# $a0 <-parameters
	move $a1, $t1		# $a1 <-parameters
	jal Ackermann
	move $a0, $v0
	li $v0, 1
	syscall
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra
Ackermann:
	beq $a0, $0, x_0	# if x = 0: go to x_0
	addi $sp, $sp, -4	# save $ra
	sw $ra, 4($sp)		# save address
	beq $a1, $0, y_0	# if y = 0: goto y_0
	addi $t0, $a0, -1
	addi $sp, $sp, -4
	sw $t0, 4($sp)		# save x- 1 in stack
	addi $a1, $a1, -1	# A(x, y-1)
	jal Ackermann
	lw $a0, 4($sp)
	addi $sp, $sp, 4
	move $a1, $v0
	jal Ackermann		# A(x-1,1)
	j end
y_0:
	addi $a0, $a0, -1	# x = x - 1		
	li $a1, 1		# y = 1
	jal Ackermann		# A(x-1, 1)
end:
	lw $ra, 4($sp)
	addi $sp, $sp, 4
	jr $ra 
x_0:
	addi $v0, $a1, 1	# return 1
	jr $ra
	