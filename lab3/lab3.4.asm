.data 0x10010000
my_array: .space 40		# reserve 40 bytes(10 words) from memory
initial_value: .word 9

.text
.globl main

main:
	lw $t1, initial_value 	# $t1 <- initial_value  
	la $t0, my_array 	# $t0 <- start address of my_array
	addi $a1, $t0, 40	# $a1 <- end address of my_array
	
loop:
	ble $a1, $t0 exit	# if $t0 >= $a1, exit 
	sw $t1, 0($t0) 	# my_arry[i] = j
	addi $t1, $t1, 1	# j++
	addi $t0, $t0, 4	# next word address position
	j loop

exit:
	jr $ra