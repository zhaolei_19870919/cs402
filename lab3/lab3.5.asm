.data 0x10010000
msg0: .asciiz "Please input int:"
msg1: .asciiz "I'm far away"
msg2: .asciiz "I'm nearby"
val: .word 1 			# address in data segment further than 16bit offset	

.text
.globl main

main:
	li $v0, 4 		# syscall print_str
	la $a0, msg0 		# address of string to print
	syscall
	
	li $v0, 5		# syscall read_int 
	syscall
	move $t0, $v0		# $t0 <- input_int
	
	li $v0, 4 		# syscall print_str
	la $a0, msg0 		# address of string to print
	syscall
	
	li $v0, 5		# syscall read_int 
	syscall
	move $t1, $v0		# $t1 <- input int
	
	beq $t0, $t1, far	# if $t0==$t1, go to "far" label			
	
	li $v0, 4		# syscall print_str
	la $a0, msg2		# address of string to print
	syscall
	jr $ra
	
far:
	li $v0, 4		# syscall print_str
	la $a0, msg1		# address of string to print
	syscall
	b val
	jr $ra