gcc basic.c -o basic.out -lm
./ basic.out small.txt
./basic.out large.txt

Result:
Num values: 12
mean: 85.776175
std: 90.380406
median: 69.064701
Unused array capacity: 8

Result:
Num values: 30
mean: 2035.600000
std: 1496.152858
median: 1629.500000