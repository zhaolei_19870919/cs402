#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "basic.h"

// open the file
FILE *openFile(char *name){
    if(fopen(name, "r") == NULL){
         printf("Can not open file\n");
         exit(0);
    }
}


int main( int argc, char *argv[] ){

    if(argc < 2){
        printf("please input .txt\n");
        return 0;
    }
    // array size is 20
    int n = 20, len_arr = 0, num = 0, num_in =0; 
    float temp;

    // allocated the memory 
    float *cur_arr = (float *) malloc(n * sizeof(float));

    char *name = argv[1];
    FILE *fp = openFile(name);

    while(!feof(fp)){
        num += 1;
        num_in += 1;
        fscanf(fp, "%f\n", (cur_arr + len_arr));
        len_arr++;
        if(len_arr == n){
            num_in -= 0;
            //create new array with twice size
            float *new_arr = (float *) malloc(2* n * sizeof(float));
            //copy values from old to new
            memcpy(new_arr, cur_arr, len_arr * sizeof(float));
            //free the memory
            free(cur_arr);
            cur_arr = new_arr;
            //increase the size of n
            n = n*2;
        }
    }
    num_in = n - num_in;
    fclose(fp);
    double mean = meanValue(cur_arr, len_arr);
    double std = stddevValue(cur_arr, len_arr);
    printf("Result:\n");
    printf("Num values: %d\n", num);
    printf("mean: %f\n", mean);
    printf("std: %f\n", std);
    sort(cur_arr, len_arr);
    double median = medianValue(cur_arr, len_arr);
    printf("median: %f\n", median);
    printf("Unused array capacity: %d\n", num_in);
    return 0;
}

