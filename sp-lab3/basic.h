#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

// get mean
double meanValue(float *p, int n){
    double sum = 0;
    for(int i=0; i<n; i++){
        sum += p[i];
    }
    double mean = sum/n;
    return mean;
}

//get stddev
double stddevValue(float *p, int n){
    //stddev = sqrt((sum((xi - mean)^2))/N);
    double std = 0;
    double mean = meanValue(p, n);
    for(int i=0; i<n; i++){
        std += pow(p[i] - mean, 2);
    }
    return sqrt(std/n);
}

//get median
double medianValue(float *cur_arr, int n){
    if(n%2 == 1){
        return (cur_arr[n/2]);
    }
    else{
        double min = cur_arr[n/2-1];
        double max = cur_arr[n/2];
        return ((min + max)/2);
    }
}

// swap two varible address
void swap(float *x, float *y){
    float temp = *x;
    *x = *y;
    *y = temp;
}

// sort array
float sort(float *cur_arr, int n){
    int i, j, min_idx;
    for(i = 0; i < n-1; i++){
        min_idx = i;
        for(j = i + 1; j < n; j++){
            if(cur_arr[j]<cur_arr[min_idx]){
                min_idx = j;
            }
            swap(&cur_arr[min_idx], &cur_arr[i]);
        }
    }
}