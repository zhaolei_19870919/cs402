import numpy as np
import time

def matrix_double_cost(row, column, count):
    totaltime = 0
    
    for i in range(count):
        a = np.random.rand(row, column)
        b = np.random.rand(column, row)
        start = time.time()
        np.dot(a, b)
        totaltime += time.time() - start
    
    return totaltime/count 
        
        
def matrix_int_cost(maxval, row, column, count):
    totaltime = 0
    
    for i in range(count):
        a = np.random.randint(maxval, size = (row, column))
        b = np.random.randint(maxval, size = (column, row))
        start = time.time()
        np.dot(a, b)
        totaltime += time.time() - start
    
    return totaltime/count 

matrix_double_cost(800, 400, 100)
matrix_int_cost(100, 800, 400, 100)

import random
import time
def matrix_create(row, column, param='int'):
    matrix = []
    for i in range(row):
        matrixin = []
        for j in range(column):
            if param == 'int':
                matrixin.append(random.randint(0,10))
            elif param == 'double':
                matrixin.append(random.random())
        matrix.append(matrixin)
    return matrix

def matrix_calculate_rowtocolumn(row, column, count, param='int'):
    totaltime = 0
  
    for _ in range(count):
        ret = []
        matrix_a = matrix_create(row, column, param)
        matrix_b = matrix_create(column, row, param)
        #print(matrix_a,matrix_b)
        start = time.time()
        for lsta in matrix_a:
            retin = []
            for i in range(row):
                lstb = []
                for line in matrix_b:
                    lstb.append(line[i])
                summ = 0
                for j in range(column):
                    summ += lsta[j]*lstb[j]
                retin.append(summ)
            ret.append(retin)
        totaltime += time.time() - start
        #print(ret)
    return totaltime/count

matrix_calculate_rowtocolumn(200, 100, 10, 'int')
matrix_calculate_rowtocolumn(200, 100, 10, 'double')