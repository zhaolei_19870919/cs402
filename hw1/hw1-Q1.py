import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
%matplotlib notebook

def address_referenced_chart(file):
    f = open(file) # open and read file
    lines = f.readlines()

    data = [] 
    data_sorted = []
    data_hex = []

    for line in lines: # read data line by line
        line = line.split() 
        data.append(int(line[1], 16)) #hex to int

    data_sorted = sorted(data, reverse=False) #sort
    for i in range(len(data_sorted)):
        data_hex.append(hex(data_sorted[i])[2:]) #int to hex

    spacing = 2000  # chart display
    fig, ax = plt.subplots(1, 1)
    plt.hist(data_hex, bins = 100)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(spacing))
    plt.show()

def frequency_calculate(file):
    f = open(file,"r") # open and read file
    lines = f.readlines()

    w_count = 0
    r_count = 0
    f_count = 0

    for line in lines: # read data line by line
        line = line.split() 
        if line[0] == '0':
            r_count += 1
        if line[0] == '1':
            w_count += 1
        if line[0]== '2':
            f_count +=1
            
    total = w_count + r_count + f_count # calculate  frequency
    print(w_count/total, r_count/total, f_count/total)

address_referenced_chart('spice.din.txt')
address_referenced_chart('tex.din.txt')
frequency_calculate('spice.din.txt')
frequency_calculate('tex.din.txt')