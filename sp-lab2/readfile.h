#include <stdio.h>
#include <string.h>
int globelStatus = 1;
FILE *fp = NULL;
struct person{
      char  lastName[12];
      char  firstName[12];
      int   salary;
      int   id;
}arr[1024], temp;

int digits(int j){
   int len = 0;
   while(j/10>0){
      len++;
      j = j/10;
   }
   return len+1;
}

int strPrint(char *str){
   int  len;
   len = strlen(str);
   printf("%s", str);
   printf("%*s\t",12-len,"");
   return 0;
}

int spaceWrite(char *str){
   int  len;
   len = strlen(str);
   return   12-len;
}

int intPrint(int i){
   int  len;
   int  j = i;
   // how many digits
   len = digits(j);
   printf("%d", i);
   printf("%*s\t",12-len,"");
   return 0;
}

int *intInputCheck(int len, int min, int max){
   int i;
   static int  a[10];
   int ret;
   ret = scanf("%d", &i);

   // check int range
   if(max!=-1 && min!= -1){
      if(i>max || i<min){
         ret = 0;
      }
   }
   a[0] = ret;
   a[1] = i;
   return a;
}

char *charInputCheck(){
   static char a[12];
   int ret;
   int i;
   ret = scanf("%s",a);
   //ret = scanf("%[^\n]%*c",a);
   // check str space("such as Jack Jones")
   for(i = 0; i < strlen(a); i++){
      if(a[i] == ' '){
         ret = 0;
      }
   }
   globelStatus = ret;
   return a;
}

int deleteEmploee(int mid, int i){
   int j = 0;
   fclose(fp);
   fp = fopen("small.txt", "w+");
   for(j=0; j<mid; j++){
         fputs(arr[j].firstName, fp); 
         int k;
         k = spaceWrite(arr[j].firstName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }

         fputs(" \t ", fp); 

         fputs(arr[j].lastName, fp); 
         k = spaceWrite(arr[j].lastName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }

         fputs(" \t ", fp);

         char s[12];
         snprintf(s, sizeof(s), "%d", arr[j].salary);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs(" \t ", fp);

         snprintf(s, sizeof(s), "%d", arr[j].id);
         fputs(s, fp); 
         fputs(" \t ", fp); 
         fputs("\n", fp); 
      }

      for(j=mid+1; j<i; j++){
         fputs(arr[j].firstName, fp); 
         int k;
         k = spaceWrite(arr[j].firstName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp); 

         fputs(arr[j].lastName, fp); 
         k = spaceWrite(arr[j].lastName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp);
         char s[12];
         snprintf(s, sizeof(s), "%d", arr[j].salary);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs(" \t ", fp);

         snprintf(s, sizeof(s), "%d", arr[j].id);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs("\n", fp); 
      }
      return 0;
}

int updateEmploee(int mid, int i){
   int j = 0;
   fclose(fp);
   fp = fopen("small.txt", "w+");
   for(j=0; j<i; j++){
         fputs(arr[j].firstName, fp); 
         int k;
         k = spaceWrite(arr[j].firstName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp); 
         fputs(arr[j].lastName, fp); 
         k = spaceWrite(arr[j].lastName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp);
         char s[12];
         snprintf(s, sizeof(s), "%d", arr[j].salary);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs(" \t ", fp);

         snprintf(s, sizeof(s), "%d", arr[j].id);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs("\n", fp); 
      }

      return 0;
}

int inputInfo(int mid, int i){
   char firstName[12];
   char lastName[12];
   int  salary, id;
   printf("Enter the first name of the employee:");
   //char[] <- char* 
   strcpy(firstName, charInputCheck());

   printf("Enter the last name of the employee:");
   //char[] <- char* 
   strcpy(lastName, charInputCheck());

   printf("Enter the salary of the employee:");
   int *p;
   // check input number is range of 30,000~150,000
   p = intInputCheck(6, 30000, 150000);
   if(*p == 1){
      salary = *(p+1);
   }
   // error message
   else{
      printf("salary is range of 30,000 ~ 150,000\n");
   }
   strcpy(arr[mid].firstName, firstName);
   strcpy(arr[mid].lastName, lastName);
   arr[mid].salary = salary;
   updateEmploee(mid, i);
   return 0;
}

int binarysearch(int i, int id, char *p){
      int low, high, mid;
      low = 0;
      high = i-1;
      //  Binary search
      while(low <= high){
         mid = (low + high)/2;
         if(id < arr[mid].id){
             high = mid - 1;
         }
         else if(id > arr[mid].id){
             low = mid + 1;
         }
         else if(p=="search" && id==arr[mid].id){
            printf("\n");
            printf("Firstname    \tLasttname   \tSalary      \tID          \n");
            strPrint(arr[mid].firstName);
            strPrint(arr[mid].lastName);
            intPrint(arr[mid].salary);
            intPrint(arr[mid].id);
            printf("\n");
            return 0;
        }
        else if(p=="remove" && id==arr[mid].id){
            deleteEmploee(mid, i);
            return 0;
        }
        else if(p=="update" && id==arr[mid].id){
            inputInfo(mid, i);
            printf("this");
            return 0;
        }
      }
      printf("not found this id in DB\n");
      return 0;
}

int arrSort(int n){
    int i;
    int j;
    for (i=0; i<n-1; ++i)
    {
        for (j=0; j<n-1-i; ++j)
        {
            if (arr[j].salary < arr[j+1].salary)
            {
                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
    return 0;
}

int salaryPrint(int i, int m){
   arrSort(i);
   printf("Firstname    \tLasttname   \tSalary      \tID          \n");
   if(m>=i){
      m=i;
   }
   for(int j = 0; j<m; j++){
      strPrint(arr[j].firstName);
      strPrint(arr[j].lastName);
      intPrint(arr[j].salary);
      intPrint(arr[j].id);
      printf("\n");
   }
   return 0;
}
