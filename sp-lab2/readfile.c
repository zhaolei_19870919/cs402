#include <stdio.h>
#include <string.h>
#include "readfile.h"


int dataIO(int param, int id, char *lastName, char *firstName, int salary){
   int i = 0;
   int ret;
   
   // open file
   char buff[255];

   // abnormal situation(no file)
   fp = fopen("small.txt", "r");
   if(fp==NULL){
      printf("Can not open file\n");
      return 0;
   }

   // param = 1: print database
   if(param == 1){ 
      printf("Firstname    \tLasttname   \tSalary      \tID          \n");
   }

   // read data and save in struct)
   while(fgets(buff, 255, (FILE*)fp)!=NULL){
      sscanf(buff,"%s %s %d %d", arr[i].firstName, arr[i].lastName, &arr[i].salary, &arr[i].id);

      if(param == 1){ 
         strPrint(arr[i].firstName);
         strPrint(arr[i].lastName);
         intPrint(arr[i].salary);
         intPrint(arr[i].id);
         printf("\n");
      }
      i++;
   }
   if(param == 1){
      printf("Number of Employees (%d) \n", i);
   }

   // param = 2: look up by id
   if(param == 2){
      binarysearch(i, id, "search");
   }

   // param = 3: look up by lastName
   if(param == 3){
      ret = 0;
      int n = 0;
      while(i>=0){
         if(strcasecmp(lastName, arr[i-1].lastName)==0){
            if(n==0){
                printf("Firstname    \tLasttname   \tSalary      \tID          \n");
            }
            strPrint(arr[i-1].firstName);
            strPrint(arr[i-1].lastName);
            intPrint(arr[i-1].salary);
            intPrint(arr[i-1].id);
            printf("\n");
            n++;
         }
         i--;
      }
      if(n==0){
         printf("not found this lastname of employee in DB\n");
      }
   }

   // param = 4: add employee info
   if(param == 4){
      int j;
      j = i;
      // find the position(comparision)
      while(j>=0){
         if(id<arr[j-1].id){
            arr[j] = arr[j-1];
            j--;
         }
       // insertion
         else{
            //char[] <- char* 
            strcpy(arr[j].firstName, firstName);
            strcpy(arr[j].lastName, lastName);
            arr[j].id = id;
            arr[j].salary = salary;
            break;
         }
      }
      fclose(fp);
      fp = fopen("small.txt", "w+");
      for(j=0; j<=i; j++){

         fputs(arr[j].firstName, fp); 
         int k;
         k = spaceWrite(arr[j].firstName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp); 

         fputs(arr[j].lastName, fp); 
         k = spaceWrite(arr[j].lastName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }

         fputs(" \t ", fp);
         
         char s[12];
         snprintf(s, sizeof(s), "%d", arr[j].salary);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs(" \t ", fp);

         snprintf(s, sizeof(s), "%d", arr[j].id);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs("\n", fp); 
      }
   }
   if(param == 5){
      binarysearch(i, id, "remove");
   }
   if(param == 6){
      binarysearch(i, id, "update");
   }
   if(param == 7){
      salaryPrint(i, salary);
   }


   fclose(fp);
   return 0;
}

int userIO(){
   int i = 0;
   int j = 0;
   int ret = 1;
   char str[12];
   globelStatus = 1;
   // terminal print function info
   printf("\n");
   printf("Employee DB Menu:\n");
   printf(" (1) Print the Database\n");
   printf(" (2) Lookup by ID\n");
   printf(" (3) Lookup by Last Name\n");
   printf(" (4) Add an Employee\n");
   printf(" (5) Remove an Employee\n");
   printf(" (6) Update an employee's information\n");
   printf(" (7) Print the M employees with the highest salaries\n");
   printf(" (8) Quit\n");

   // seleclt function
   printf("Enter your choice:");
   ret = scanf("%d", &i);
   printf("\n");
   
   if(ret==1){
      if(i==1){
         dataIO(i, 0, "", "", 0);
      }

      // look up by ID
      else if(i==2){
         printf("Enter a 6 digit employee id: ");
         int *p;
         p = intInputCheck(6, 100000, 999999);
         // check input number is range of 100,000~999,999
         if(*p == 1){
            *(p+1);
            dataIO(i, *(p+1), "", "", 0);
         }
         // error message
         else{
            printf("ID is  of 100,000 ~ 999,999\n");
         }
      }
      else if(i==3){
         printf("Enter Employee's last name (no extra spaces):");
         char *str;
         str = charInputCheck();
         // check input has no extra spaces
         if(globelStatus == 1){
           dataIO(i, 0, str, "", 0);
         }
         // error message
         else{
            printf("Enter last name must be no extra spaces:");
         }
      }
      else if(i==4){
         char firstName[12];
         char lastName[12];
         int  salary, id;

         printf("Enter the first name of the employee:");
         //char[] <- char* 
         strcpy(firstName, charInputCheck());
      
         printf("Enter the last name of the employee:");
         //char[] <- char* 
         strcpy(lastName, charInputCheck());

         printf("Enter the salary of the employee:");
         int *p;
         // check input number is range of 30,000~150,000
         p = intInputCheck(6, 30000, 150000);
         if(*p == 1){
            salary = *(p+1);
         }
         // error message
         else{
            printf("salary is range of 30,000 ~ 150,000\n");
            scanf("%*[^\n]%*c");// clean
            userIO();
            return 0;
         }

         // check input number is range of 100,000~999,999
         printf("Enter the id of the employee:");
         p = intInputCheck(6, 100000, 999999);
         if(*p == 1){
            id = *(p+1);
         }
         // error message
         else{
            printf("ID is range of 100,000 ~ 999,999\n");
            scanf("%*[^\n]%*c");// clean
            userIO();
            return 0;
         }

         dataIO(i, id, lastName, firstName, salary);
      }
      else if(i==5){
         printf("Enter a 6 digit employee id: ");
         int *p;
         p = intInputCheck(6, 100000, 999999);
         // check input number is range of 100,000~999,999
         if(*p == 1){
            *(p+1);
            dataIO(i, *(p+1), "", "", 0);
         }
         // error message
         else{
            printf("ID is  of 100,000 ~ 999,999\n");
         }
      }
      else if(i==6){
         printf("Enter a 6 digit employee id: ");
         int *p;
         p = intInputCheck(6, 100000, 999999);
         // check input number is range of 100,000~999,999
         if(*p == 1){
            *(p+1);
            dataIO(i, *(p+1), "", "", 0);
         }
         else{
            printf("ID is  of 100,000 ~ 999,999\n");
         }
      }
      else if(i==7){
         printf("Print the M employees with the highest salaries:");
         int *p;
         p = intInputCheck(6, 1, 1024);
         if(*p == 1){
            *(p+1);
            dataIO(i, 0, "", "", *(p+1));
         }
         else{
            printf("Input M number should be of 1 ~ 1024\n");
         }
      }
      else if(i==8){// userIO close...
         printf("goodbye!\n");
         return 0;
      }

      else{// abnormal situation(such as input 6)
         printf("please input 1~5\n");
      }
   }
   else{// abnormal situation(such as input "abc")
      printf("please input number\n");
   }
   scanf("%*[^\n]%*c");// clean
   userIO();
   return 0;
}

int  main(){
   userIO();
   return 0;
}

