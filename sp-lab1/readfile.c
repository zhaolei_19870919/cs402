#include <stdio.h>
#include <string.h>
#include "readfile.h"


int dataIO(int param, int id = 0, char *lastName = "", char *firstName = "", int salary = 0){
   int i = 0;
   int ret;

   struct person{
      char  lastName[12];
      char  firstName[12];
      int   salary;
      int   id;
   }arr[1024], temp;

   // open file
   FILE *fp = NULL;
   char buff[255];

   // abnormal situation(no file)
   fp = fopen("small.txt", "r");
   if(fp==NULL){
      printf("Can not open file\n");
      return 0;
   }

   // param = 1: print database
   if(param == 1){ 
      printf("Firstname    \tLasttname   \tSalary      \tID          \n");
   }

   // read data and save in struct)
   while(fgets(buff, 255, (FILE*)fp)!=NULL){
      sscanf(buff,"%s %s %d %d", arr[i].firstName, arr[i].lastName, &arr[i].salary, &arr[i].id);

      if(param == 1){ 
         strPrint(arr[i].firstName);
         strPrint(arr[i].lastName);
         intPrint(arr[i].salary);
         intPrint(arr[i].id);
         printf("\n");
      }
      i++;
   }
   if(param == 1){
      printf("Number of Employees (%d) \n", i);
   }

   // param = 2: look up by id
   if(param == 2){
      int low, high, mid;
      low = 0;
      high = i-1;
      //  Binary search
      while(low < high){
         mid = (low + high)/2;
         if(id < arr[mid].id){
             high = mid - 1;
         }
         else if(id > arr[mid].id){
             low = mid + 1;
         }
         else if(id==arr[mid].id){
            printf("\n");
            printf("Firstname    \tLasttname   \tSalary      \tID          \n");
            strPrint(arr[mid].firstName);
            strPrint(arr[mid].lastName);
            intPrint(arr[mid].salary);
            intPrint(arr[mid].id);
            printf("\n");
            return 0;
        }
      }
      printf("not found this id in DB\n");
   }

   // param = 3: look up by first name
   if(param == 3){
      ret = 0;
      while(i>=0){
         if(strcmp(lastName, arr[i-1].lastName)==0){
            printf("Firstname    \tLasttname   \tSalary      \tID          \n");
            strPrint(arr[i-1].firstName);
            strPrint(arr[i-1].lastName);
            intPrint(arr[i-1].salary);
            intPrint(arr[i-1].id);
            printf("\n");
            return 0;
         }
         i--;
      }
      printf("not found this lastname of employee in DB\n");
   }

   // param = 4: add employee info
   if(param == 4){
      int j;
      j = i;

      // find the position(comparision)
      while(j>=0){
         if(id<arr[j-1].id){
            arr[j] = arr[j-1];
            j--;
         }
       // insertion
         else{
            //char[] <- char* 
            strcpy(arr[j].firstName, firstName);
            strcpy(arr[j].lastName, lastName);
            arr[j].id = id;
            arr[j].salary = salary;
            break;
         }
      }
      fclose(fp);
      fp = fopen("small.txt", "w+");
      for(j=0; j<=i; j++){

         fputs(arr[j].firstName, fp); 
         int k;
         k = spaceWrite(arr[j].firstName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }
         fputs(" \t ", fp); 

         fputs(arr[j].lastName, fp); 
         k = spaceWrite(arr[j].lastName);
         while(k>=0){
            fputs(" ", fp); 
            k--;
         }

         char s[12];
         snprintf(s, sizeof(s), "%d", arr[j].salary);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         snprintf(s, sizeof(s), "%d", arr[j].id);
         fputs(s, fp); 
         fputs(" \t ", fp); 

         fputs("\n", fp); 
      }
   }

   fclose(fp);
   return 0;
}

int userIO(){
   int i = 0;
   int j = 0;
   int ret = 1;
   char str[12];
   globelStatus = 1;
   // terminal print function info
   printf("\n");
   printf("Employee DB Menu:\n");
   printf(" (1) Print the Database\n");
   printf(" (2) Lookup by ID\n");
   printf(" (3) Lookup by Last Name\n");
   printf(" (4) Add an Employee\n");
   printf(" (5) Quit\n");

   // seleclt function
   printf("Enter your choice:");
   ret = scanf("%d", &i);
   printf("\n");
   
   if(ret==1){
      if(i==1){
         dataIO(i);
      }

      // look up by ID
      else if(i==2){
         printf("Enter a 6 digit employee id: ");
         int *p;
         p = intInputCheck(6, 100000, 999999);
         // check input number is range of 100,000~999,999
         if(*p == 1){
            *(p+1);
            dataIO(i, *(p+1));
         }
         // error message
         else{
            printf("ID is  of 100,000 ~ 999,999\n");
         }
      }
      else if(i==3){
         printf("Enter Employee's last name (no extra spaces):");
         char *str;
         str = charInputCheck();
         // check input number is range of 100,000~999,999
         if(globelStatus == 1){
           dataIO(i, 0, str);
         }
         // error message
         else{
            printf("Enter last name must be no extra spaces:");
         }
      }
      else if(i==4){
         char firstName[12];
         char lastName[12];
         int  salary, id;

         printf("Enter the first name of the employee:");
         //char[] <- char* 
         strcpy(firstName, charInputCheck());
      
         printf("Enter the last name of the employee:");
         //char[] <- char* 
         strcpy(lastName, charInputCheck());

         printf("Enter the salary of the employee:");
         int *p;
         // check input number is range of 30,000~150,000
         p = intInputCheck(6, 30000, 150000);
         if(*p == 1){
            salary = *(p+1);
         }
         // error message
         else{
            printf("salary is range of 30,000 ~ 150,000\n");
            scanf("%*[^\n]%*c");// clean
            userIO();
            return 0;
         }

         // check input number is range of 100,000~999,999
         printf("Enter the id of the employee:");
         p = intInputCheck(6, 100000, 999999);
         if(*p == 1){
            id = *(p+1);
         }
         // error message
         else{
            printf("ID is range of 100,000 ~ 999,999\n");
            scanf("%*[^\n]%*c");// clean
            userIO();
            return 0;
         }

         dataIO(i, id, lastName, firstName, salary);
      }
      else if(i==5){// userIO close...
         printf("goodbye!\n");
      }

      else{// abnormal situation(such as input 6)
         printf("please input 1~5\n");
      }
   }
   else{// abnormal situation(such as input "abc")
      printf("please input number\n");
   }
   scanf("%*[^\n]%*c");// clean
   userIO();
   return 0;
}

int  main(){
   userIO();
   return 0;
}