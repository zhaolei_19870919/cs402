# Summary #
There are three documents:
readfile.c
readflic.h
small.txt

Please make sure they are in a folder, using the C / C + + compiler, run readfile. C,  the program can be executed.(It is recommended that you use the IDE of vscode).

# Detail #
This database project consists of three parts：
userIO
dataIO 
Functions

#1.userInput&Output: Used to manage user I/O interactions,Such as:#
- Get the user's input information in Terminal;
- Display the information after data processing
- Control user's entry and exit process (by recursive call);
...

#2.dataInput&Output:Used to Used to process files and data, Such as:#
- Open & read & write & close the file;
- Use the data structure to structure text
- Use sorting and search algorithms to process issues;
- Interact with user's request and return result;
...

#3.check&print function:Combine the repeated code to functions, Such as:#
- Space character output method  is used for neat print;
- Validations that conform to data rules, such as salary & ID ranges;
- Verification of string & integer & space ..
...