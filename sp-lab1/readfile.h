#include <stdio.h>
#include <string.h>

int globelStatus = 1;


int digits(int j){
   int len = 0;
   while(j/10>0){
      len++;
      j = j/10;
   }
   return len+1;
}

int strPrint(char *str){
   int  len;
   len = strlen(str);
   printf("%s", str);
   printf("%*s\t",12-len,"");
   return 0;
}

int spaceWrite(char *str){
   int  len;
   len = strlen(str);
   return   12-len;
}



int intPrint(int i){
   int  len;
   int  j = i;
   // how many digits
   len = digits(j);
   printf("%d", i);
   printf("%*s\t",12-len,"");
   return 0;
}

int *intInputCheck(int len = -1, int min = -1, int max= -1){
   int i;
   static int  a[10];
   int ret;
   ret = scanf("%d", &i);

   // check int range
   if(max!=-1 && min!= -1){
      if(i>max || i<min){
         ret = 0;
      }
   }
   a[0] = ret;
   a[1] = i;
   return a;
}

char *charInputCheck(){
   static char a[12];
   int ret;
   int i;
   ret = scanf("%s",a);
   //ret = scanf("%[^\n]%*c",a);
   // check str space("such as Jack Jones")
   for(i = 0; i < strlen(a); i++){
      if(a[i] == ' '){
         ret = 0;
      }
   }
   globelStatus = ret;
   return a;
}